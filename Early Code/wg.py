import turtle
import random

#turtle.ht()
drawer = turtle.Turtle()
turtle.title("RWG")
drawer.speed(1000)
drawer.hideturtle()


def square():
    drawer.forward(5)
    drawer.left(90)
    drawer.forward(5)
    drawer.left(90)
    drawer.forward(5)
    drawer.left(90)
    drawer.forward(5)


def window():
    drawer.color("white")
    drawer.begin_fill()
    square()
    drawer.end_fill()


def wooden_door():
    drawer.color("brown")
    drawer.begin_fill()
    square()
    drawer.left(270)
    square()
    square()
    square()
    square()
    drawer.right(90)
    drawer.forward(5)
    square()
    square()
    drawer.end_fill()


def stone_door():
    drawer.color("dark gray")
    drawer.begin_fill()
    square()
    drawer.left(270)
    square()
    square()
    square()
    square()
    drawer.right(90)
    drawer.forward(5)
    square()
    square()
    drawer.end_fill()


def wood():
    drawer.color("brown")
    drawer.begin_fill()
    square()
    drawer.end_fill()


def stone():
    drawer.color("dark gray")
    drawer.begin_fill()
    square()
    drawer.end_fill()


def wooden_house():
    wooden_door()

for i in range(1000):
    x = random.randrange(1,400)
    
turtle.screensize()

turtle.exitonclick()
