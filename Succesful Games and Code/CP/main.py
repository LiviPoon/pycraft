from tkinter import*
import importlib
import Chemical_Project


class Executor:
    def __init__(self):
        self.root = Tk()

        self.app = Chemical_Project.Hello(master=self.root, on_reload=self.on_reload)
        self.app.mainloop()

    def on_reload(self):
        self.root.destroy()

        importlib.reload(Chemical_Project)

        self.root = Tk()
        self.app = Chemical_Project.Hello(master=self.root, on_reload=self.on_reload)
        self.app.mainloop()


if __name__ == '__main__':
    Executor()
