from tkinter import *
import random
import webbrowser
import time
class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class Hello(Frame):

    def shut(self):
        print (bcolors.WARNING + "Boot Shutdown, Confirmed." + bcolors.ENDC)
        print (bcolors.WARNING + "Please wait..." + bcolors.ENDC)
        time.sleep(10)
        self.root.destroy()
        self.root2.destroy()
        self.root3.destroy()
        self.root4.destroy()
        self.root5.destroy()
        self.root6.destroy()
        self.root7.destroy()
        self.root8.destroy()

    def callback(self, event):#Function for buttons to click to this certain link.
        webbrowser.open_new(r"https://my.nuevaschool.org/base.php?q__=jOoNcOJUa47Lw%2F%2Bma4L77JhW6pY3ybHV0I%2Fw2YYqEV7tTgzkPpZxkkHWIDxyogKq")
        webbrowser.open_new(r"https://docs.google.com/document/d/1iubfsk8w7dMqe-PFZAEmhtU-Y7zsRwlXxrQWmDegJL4/edit")
    def callback2(self, event):#Again to click to this certain link.
        webbrowser.open_new(r"https://my.nuevaschool.org/base.php?q__=jOoNcOJUa47Lw%2F%2Bma4L77JhW6pY3ybHV0I%2Fw2YYqEV4WkcancYCXy1omoqW9okS3")
    def INFO(self, event):
        root3 = Tk()
        TEXT = open("BakingSoda_Vinegar_Reaction.txt", "r+").readlines()
        #An explaination of how and why the reaction of Baking Soda and Vinegar works.
        text = Label(root3,font=("Courier New", 12), text=TEXT, justify= LEFT)
        text.pack()             #Sets the font

    def vinegar1(self, event):
        root5 = Tk()
        root5.wm_title("ProChem. Vinegar")
        TEXT = open("Vinegar.txt", "r+").readlines()
        text = Label(root5, font=("Courier New", 12), text=TEXT, justify= LEFT)
        text.pack()

    def bakingSoda1(self, event):
        root5 = Tk()
        root5.wm_title("ProChem. Baking Soda") #Makes sure that the tile of that screen is ProChem. Baking Soad
        TEXT = open("Baking_Soda", "r+").readlines()

    def flour1(self, event):
        root5 = Tk()
        root5.wm_title("ProChem. Baking Soda")
        TEXT = open("Flour.txt", "r+").readlines()
        photo = PhotoImage(file="1.gif")
        label = Label(root5, image=photo)
        label.image = photo # keep a reference!
        label.pack()
        bakingSoda_1 = Label(root5, font=("Courier New", 13), text= TEXT, justify= LEFT)
        bakingSoda_1.pack()

    def info(self): #Makes sure that the information on each element that is given has been executed.
        if "flour" in self.rand_elem and "A" or "a" in self.level:
            root7 = Tk()
            root7.wm_title("ProChem. Flour")
            flour_t = open("Flour.txt", "r+").readlines() #Opens file Flour.txt and reads everything on that file.
            flour = Label(root7, font=("Courier New", 13), text = flour_t, justify= LEFT) #Makes a label that has a font of Courier New
            flour.pack()
        else:
            print (bcolors.FAIL +  "Error, line 65. Flour not comprehended" + bcolors.ENDC) #For troubleshooting purposes

        if "baking soda" in self.rand_elem and "A" or "a" in self.level:
            root8 = Tk()
            root8.wm_title("ProChem. Baking Soda")
            baking_soda_t = open("Baking_Soda.txt", "r+").readlines()
            baking_soda = Label(root8, font=("Courier New", 13), text = baking_soda_t, justify= LEFT)
            baking_soda.pack()
        else:
            print (bcolors.FAIL + "Error, line 54. Baking Soda not comprehended" + bcolors.ENDC)

        if "vinegar" in self.rand_elem and "A" or "a" in self.level:
            root9 = Tk()
            root9.wm_title("ProChem. Vinegar")
            vinegar_t = open("Vinegar.txt", "r+").readlines()
            vinegar = Label(root9, font=("Courier New", 13), text = vinegar_t, justify= LEFT)
            vinegar.pack()
        else:
            print (bcolors.FAIL + "Error, line 63. Vinegar not comprehended" + bcolors.ENDC)

        print (bcolors.BOLD + "To make a chemical reaction enter your first and second elements in the order fo-lowing this message" + bcolors.ENDC)
        time.sleep(10)

        inr = input (bcolors.UNDERLINE + "First Element:\n " + bcolors.ENDC)
        inr1 = input (bcolors.UNDERLINE + "Second Element:\n " + bcolors.ENDC)
        if "vinegar" in inr or "baking soda" and "vinegar" in inr1 or "baking soda" in inr1:
            print   ("   ")
            print ("You have succesfully entered the right elements, you have just formed a chemical reaction.")
        else:
            print (bcolors.FAIL + "Error, The Elements That you have Entered are not Comprehended" + bcolors.ENDC)




    def reactions(self, event):
        vinigar_bakingSoda = ["vinegar", "baking soda"]
        root6 = Tk()
        start_Text = Label(root6, text = "This is a TesT, please enter your level into the terminal box below.", justify= LEFT, font=("Courier New", 12))
        start_Text.pack()
        self.level = input(bcolors.HEADER + "Please pick a Level between A - E, E being the hardest: " + bcolors.ENDC)
        start_Text2 = Label(root6, text = """
        The folowing level is what you have entered in below.
        We will now begin, what this test will do is it will give you a bunch of either windows or links for
        you to click on. These windows or links will give you information on the elements to which are given randomly.
        using the information given you must make a deduce a way to make a chemical reaction. The amount of
        information given will be determined by the level at which you have decided to choose.""",justify= LEFT, font=("Courier New", 12))
        start_Text2.pack()
        print (bcolors.BOLD + "Loading." + bcolors.ENDC)
        time.sleep(5)
        print (bcolors.BOLD + "Loading.." + bcolors.ENDC)
        time.sleep(5)
        print (bcolors.BOLD + "Loading..." + bcolors.ENDC)
        print ("""



















        """)
        print (bcolors.HEADER + "Session Beginning, Level:" + bcolors.ENDC)
        print (self.level)
        elements = ["vinegar", "baking soda", "flour", "Azodicarbonamide"]
        for i in range(10):
            global x
            self.rand_elem = random.sample(elements, 3)
            if "vinegar" in self.rand_elem and "baking soda" in self.rand_elem :
                print(self.rand_elem)
                self.info()
                break

        else:
            print ("Error")

    def start(self, event):
        root4 = Tk()
        level_1 = Label(root4, font=("Courier New", 13), text="Vinegar", fg = "dark green", cursor="hand" )
        level_1.pack()
        level_1.bind("<Button-1>", self.vinegar1)
        level_1 = Label(root4, font=("Courier New", 13), text="Baking Soda", fg = "dark green", cursor="hand" )
        level_1.pack()
        level_1.bind("<Button-1>", self.flour1)
    def creators(self, event):
        text = """
.-.    _        _   .---.
: :   :_;      :_;  : .; :
: :   .-..-..-..-.  :  _.'.--.  .--. ,-.,-.
: :__ : :: `; :: :  : :  ' .; :' .; :: ,. :
:___.':_;`.__.':_;  :_;  `.__.'`.__.':_;:_;

                                           """
        print (text)
        text4 = """
_ /\ |  _|_  _.._ |_) _.._ _ |    ._ _  _.._
 /--\|<_>| |(_||  | \(_|| | ||<|_|| | |(_||

"""
        print (text4)
        text7 = """

.-. .-. .-. . .   . . . .-. .-. .-. .-.   .-. .-.  . .-.
|   | | |-'  |    | | | |(   |   |  |-    .'' |\| '| |-.
`-' `-' '    `    `.'.' ' ' `-'  '  `-'   `-- `-'  ' `-' ,

.-. .-. .-. .-. .-. .-. .-.   .-. .-.
| | |    |  | | |(  |-  |(    .'' `-|
`-' `-'  '  `-' `-' `-' ' '   `-- `-' .
"""
        print (text7)

    def START(self, event):
        #What the screen shows when the ProChem. Logo is pressed.
        root2 = Tk()
        root2.wm_title("ProChem. Baking Soda")
        VINEGER = Label(root2,font=("Courier New", 13),text="Reactions_INFO",cursor="hand")
        VINEGER.pack() #Making the label show on.
        VINEGER.bind("<Button-1>", self.INFO) #Binding the mouse button to the label.
        ELEMENTS = Label(root2,font=("Courier New", 13),text="Elements",cursor="hand")
        ELEMENTS.pack()
        ELEMENTS.bind("<Button-1>", self.start)
        ELEMENTS_INFO = Label(root2,font=("Courier New", 13),text="Creators",cursor="hand")
        ELEMENTS_INFO.pack()
        ELEMENTS_INFO.bind("<Button-1>", self.creators)
        ELEMENTS_INFO = Label(root2,font=("Courier New", 13),text="Start",cursor="hand")
        ELEMENTS_INFO.pack()
        ELEMENTS_INFO.bind("<Button-1>", self.reactions)





    def __init__(self, master=None, on_reload= None):
        Frame.__init__(self,master) #The main screen
        self.root = master
        self.master = master
        self.root.wm_title("ProChem.")

        self.TITLE = Label(text="ProChem.", font=("Courier New", 16), cursor="hand")
        self.TITLE.pack()
        self.TITLE.bind("<Button-1>", self.START)
        self.CREATORS = Label(text="Livi Poon", font=("Courier New", 10), fg="blue", cursor="hand")
        self.CREATORS.pack()
        self.CREATORS.bind("<Button-1>", self.callback)
        self.CREATORS = Label(text="Akshar Ramkumar", font=("Courier New", 10), fg="blue", cursor="hand")
        self.CREATORS.pack()
        self.CREATORS.bind("<Button-1>", self.callback2)
        self.CREATORS = Label(text="Copyright, 2016. Pyrcraft", font=("Courier New", 7))
        self.CREATORS.pack()
        self.my_reload = Button(self.root)
        self.my_reload["text"] = "ReLaunch",
        self.my_reload["command"] = on_reload
        self.my_reload.pack()

        self.command = input("Command line: ")
        self.root.config(width=400, height=400)
        if self.command == "quit" or self.command == "Quit":
            self.shut()
        else:
            print (bcolors.FAIL + "Error, process terminated" + bcolors.ENDC)


if __name__ == '__main__':
        app = Hello(Tk())
        app.mainloop()
