from Tkinter import *
import time
import random
import os


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

class Lander:
    def __init__(self, game,canvas, SavedCoords, fuel_Number,paused,lander_x1,lander_x2,saved_coords, continue_tick):
        self.crash = False
        self.land = False
        game_started= True
        L1_engine_down_kb = '<KeyPress-Down>'
        L1_engine_left_kb = '<KeyPress-Left>'
        L1_engine_right_kb = '<KeyPress-Right>'
        self.game = game#accesing all of the specs in the game class to be the specs in the Lander class.
        self.canvas = canvas
        self.id = self.canvas.create_rectangle(lander_x1, 0, lander_x2, 20, outline = 'Blue')
        self.time = time.time()
        self.x = 0
        self.y = 0.01
        self.engine_y = 0
        self.engine_x = 0
        self.fuel = fuel_Number
        self.down = False
        self.left = False
        self.right = False

        self.canvas.bind_all(L1_engine_down_kb, self.L1_engine_down)
        self.canvas.bind_all(L1_engine_left_kb, self.L1_engine_left)
        self.canvas.bind_all(L1_engine_right_kb, self.L1_engine_right)

        self.engine_y_text = self.canvas.create_text(0, 40,font=("Courier New", 12), text='Main Engine: off', anchor = 'nw')
        self.engine_x_text = self.canvas.create_text(0, 60,font=("Courier New", 12), text='Thrusters: off', anchor = 'nw')
        self.engine_fuel_text = self.canvas.create_text(0, 80,font=("Courier New", 12), text='Fuel: %s' % self.fuel, anchor = 'nw')
        self.x_text = self.canvas.create_text(0, 100,font=("Courier New", 12), text='x: %s' % self.x, anchor = 'nw')
        self.y_text = self.canvas.create_text(0, 120,font=("Courier New", 12), text='y: %s' % self.y, anchor = 'nw')

    def Start_Screen_Boot(self, evt, paused, SavedCoords):
        self.canvas.delete("all")
        start_screen = Start_Screen(self.game)
        paused = True
        SavedCoords = True

    def L1_engine_down(self, evt):
        if self.down:
            self.canvas.itemconfig(self.engine_y_text, text='Main Engine: off')
        else:
            self.canvas.itemconfig(self.engine_y_text, text='Main Engine: on')
        self.engine_down(evt)

    def L1_engine_left(self, evt):
        if self.left:
            self.canvas.itemconfig(self.engine_x_text, text='Thrusters: off')
        else:
            self.canvas.itemconfig(self. engine_x_text, text='Thrusters: left')
        self.engine_left(evt)

    def L1_engine_right(self, evt):
        if self.right:
            self.canvas.itemconfig(self.engine_x_text, text='Thrusters: off')
        else:
            self.canvas.itemconfig(self.engine_x_text, text='Thrusters: right')
        self.engine_right(evt)

    def move(self, game, paused, SavedCoords,GRAVITY,ENGINE_POWER, THRUSTER_POWER, MAXIMUM_ENGINE_POWER, MAXIMUM_THRUSTER_POWER, lander_x1, lander_x2, running): #MOVING THE LANDER FROM SIDE TO SIDE
#        if self.canvas.coords(self.id[3]) >= 480:
#            if self.canvas.coords(self.id[3]) <= 490:
#                print(find_overlapping(self.x1, self.y1, self.x2, self.y2))
        def Start_Screen_Boot(self, evt, paused, SavedCoords):
            self.canvas.delete("all")
            start_screen = Start_Screen(self.game)
            paused = True
            SavedCoords = True

        if running == True:
            if self.canvas.coords(self.id)[3] >= 500:
                self.id = self.canvas.create_rectangle(lander_x1, 0, lander_x2, 20, outline = 'Blue')
                if self.y > 1:
                    self.crash = True
                    continue_tick = 0
                else:
                    self.land = True
                    continue_tick = 0
                return



        now = time.time()
        time_since_last = now - self.time

        if time_since_last > 0.1:
            if self.down and self.engine_y > MAXIMUM_ENGINE_POWER:
                self.engine_y += ENGINE_POWER

            if self.left and self.engine_x < MAXIMUM_THRUSTER_POWER:
                self.engine_x += THRUSTER_POWER
            elif self.right and self.engine_x > -MAXIMUM_THRUSTER_POWER:
                self.engine_x -= THRUSTER_POWER

            if self.down: #main fuel
                self.fuel -= 2

            if self.left or self.right: #left or right boosters
                self.fuel -= 1

            if self.fuel < 0:
                self.fuel = 0
            self.id_coords = self.canvas.coords(self.id)
            self.canvas.itemconfig(self.engine_fuel_text, text='Fuel: %s' % self.fuel)
            self.canvas.itemconfig(self.x_text, text='x: %s' % self.x)
            self.canvas.itemconfig(self.y_text, text='y: %s' % self.y)


            if self.fuel <= 0:
                self.engine_y = 0
                self.engine_x = 0
            self.y = self.y + (time_since_last * GRAVITY) + (time_since_last * self.engine_y)
            self.x = self.x + (time_since_last * self.engine_x)
            self.canvas.move(self.id, self.x, self.y)

    def engine_down(self, evt):
        if self.down:
            self.down = False
            self.engine_y = 0
        else:
            self.down = True

    def engine_left(self, evt):
        if self.left:
            self.left = False
            self.right = False
            self.engine_x = 0
        else:
            self.left = True

    def engine_right(self, evt):
        if self.right:
            self.left = False
            self.right = False
            self.engine_x = 0
        else:
            self.right = True
