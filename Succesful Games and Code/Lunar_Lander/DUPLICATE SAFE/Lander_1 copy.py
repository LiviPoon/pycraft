from Tkinter import *
import time
import random
import os

class Lander:
    def __init__(self, game, SavedCoords, fuel_Number):
        game_started= True
        self.game = game#accesing all of the specs in the game class to be the specs in the Lander class.
        self.canvas = game.canvas
        self.title_text = self.canvas.create_text(550, 10,font=("Courier New", 20), text='Lunar Lander', anchor= 'nw')
        self.by = self.canvas.create_text(550,30, font=('Courier New', 10), text='By Livi Poon', anchor = 'nw')
        if SavedCoords == False:
            self.id = self.canvas.create_rectangle(390, 0, 410, 20)
            self.time = time.time()
            self.x = 0
            self.y = 0.01
            self.engine_y = 0
            self.engine_x = 0
            self.fuel = fuel_Number
        else:
            self.id = self.canvas.create_rectangle(saved_coords[0], saved_coords[1], saved_coords[2], saved_coords[3])
            self.time = time.time()
            self.x = 0
            self.y = saved_y
            self.engine_y = 0
            self.engine_x = 0
            self.fuel = saved_fuel
        self.down = False
        self.left = False
        self.right = False
        self.game.canvas.bind_all('<KeyPress-Down>', self.engine_down)
        self.game.canvas.bind_all('<KeyPress-Left>', self.engine_left)
        self.game.canvas.bind_all('<KeyPress-Right>', self.engine_right)

        self.player2 = self.canvas.create_text(0,20, font=("Courier New", 14), text='Player 1', fill="blue", anchor= 'nw')
        self.engine_y_text = self.canvas.create_text(0, 40,font=("Courier New", 12), text='Main Engine: off', fill = "blue", anchor = 'nw')
        self.engine_x_text = self.canvas.create_text(0, 60,font=("Courier New", 12), text='Thrusters: off', fill = "blue", anchor = 'nw')
        self.engine_fuel_text = self.canvas.create_text(0, 80,font=("Courier New", 12), text='Fuel: %s' % self.fuel, fill = "blue",anchor = 'nw')

        self.x_text = self.canvas.create_text(0, 100,font=("Courier New", 12), text='x: %s' % self.x, fill = "blue", anchor = 'nw')
        self.y_text = self.canvas.create_text(0, 120,font=("Courier New", 12), text='y: %s' % self.y,  fill = "blue", anchor = 'nw')


    def callback(self, evt, SavedCoords):
        self.game.canvas.delete("all")
        lander = Lander(self.game)
        lander_2 = Lander_Corrupt(self.game)
        platfrom = Platform(self.game)
        self.game.sprites = []
        self.game.sprites.append(lander_2)
        self.game.sprites.append(lander)
        SavedCoords = True

    def mainloop(self): #updating the postition of the Lunar Lander
        game.mainloop()

    def Start_Screen_Boot(self, evt, paused, SavedCoords):
        self.game.canvas.delete("all")
        start_screen = Start_Screen(self.game)
        paused = True
        SavedCoords = True


    def Continue(self, game, pause_tick, paused, SavedCoords):
        pause_tick = 0
        SavedCoords = True
        self.callback(game)
        paused = False


    def Main(self, game):
        self.Start_Screen_Boot(game)

    def Pause(self, game, continue_tick = 0, pause_tick = 0):
        pause_tick +=1
        continue_tick +=1
        global saved_y
        global saved_fuel
        global saved_engine_y
        global saved_engine_x
        saved_coords = self.id_coords
        saved_fuel = self.fuel
        saved_y = self.y
        saved_engine_y = self.engine_y
        saved_engine_x = self.engine_x
        if pause_tick == 2:
            os.system("clear")
            self.main = self.canvas.create_text(350,275, font=('Courier New', 15), activefill="green",text ='Main Menu',fill="black",tag='main')
            self.main = self.canvas.create_text(350,300, font=('Courier New', 15), activefill="green",text ='Continue',fill="black",tag='continue')
            self.canvas.tag_bind( , '<ButtonPress-1>', self.Continue)
        else:
            self.canvas.delete("all")
            self.paused = self.canvas.create_text(350,200, font=("Courier New", 28), fill='maroon1', text="PAUSED")
            self.title_text = self.canvas.create_text(550, 10,font=("Courier New", 20), text='Lunar Lander', anchor= 'nw')
            self.by = self.canvas.create_text(550,30, font=('Courier New', 10), text='By Livi Poon', anchor = 'nw')
            global paused
            paused = True
            self.pause = self.canvas.create_text(685, 490,font=("Courier New", 12),activefill= 'green', text='||',  fill = "black", tag='pause')
            self.canvas.tag_bind('pause', '<ButtonPress-1>', self.Pause)





    def move(self, game, paused, SavedCoords,GRAVITY,ENGINE_POWER, THRUSTER_POWER, MAXIMUM_ENGINE_POWER, MAXIMUM_THRUSTER_POWER): #MOVING THE LANDER FROM SIDE TO SIDE
#        if self.canvas.coords(self.id[3]) >= 480:
#            if self.canvas.coords(self.id[3]) <= 490:
#                print(find_overlapping(self.x1, self.y1, self.x2, self.y2))
        if self.canvas.coords(self.id)[3] >= 500:
            try:
                if self.y > 1:
                    print("")
                    paused = True
                    print(bcolors.FAIL + 'BOOM, you have crashed!' + bcolors.ENDC)
                    self.canvas.delete("all")
                    self.crashed = self.canvas.create_text(350,350, font=("Courier New", 24), fill='red', text="YOU HAVE CRASHED")
                    self.main = self.canvas.create_text(350,375, font=("Courier New", 12), activefill='green',text="Mainmenu", tag='main')
                    self.canvas.tag_bind('main', '<ButtonPress-1>', self.Start_Screen_Boot)
                    print("")
                else:
                    print("")
                    paused = True
                    SavedCoords = False
                    print(bcolors.HEADER + 'Safe Landing' +  bcolors.ENDC)
                    self.canvas.delete("all")
                    self.safe = self.canvas.create_text(350,350, font=("Courier New", 24), fill='maroon1', text="YOU HAVE LANDED")
                    self.main = self.canvas.create_text(350,375, font=("Courier New", 12), activefill='green',text="Mainmenu", tag='main')
                    self.canvas.tag_bind('main', '<ButtonPress-1>', self.Start_Screen_Boot)
                    print("")
                return
            except:
                #print("self.id is {}".format(self.id))
                raise



        now = time.time()
        time_since_last = now - self.time

        if time_since_last > 0.1:
            if self.down and self.engine_y > MAXIMUM_ENGINE_POWER:
                self.engine_y += ENGINE_POWER

            if self.left and self.engine_x < MAXIMUM_THRUSTER_POWER:
                self.engine_x += THRUSTER_POWER
            elif self.right and self.engine_x > -MAXIMUM_THRUSTER_POWER:
                self.engine_x -= THRUSTER_POWER

            if self.down: #main fuel
                self.fuel -= 2

            if self.left or self.right: #left or right boosters
                self.fuel -= 1

            if self.fuel < 0:
                self.fuel = 0
            self.id_coords = self.canvas.coords(self.id)
            self.canvas.itemconfig(self.engine_fuel_text, text='Fuel: %s' % self.fuel)
            self.canvas.itemconfig(self.x_text, text='x: %s' % self.x)
            self.canvas.itemconfig(self.y_text, text='y: %s' % self.y)


            if self.fuel <= 0:
                self.engine_y = 0
                self.engine_x = 0
            self.y = self.y + (time_since_last * GRAVITY) + (time_since_last * self.engine_y)
            self.x = self.x + (time_since_last * self.engine_x)
            self.canvas.move(self.id, self.x, self.y)



        self.id_coords = self.canvas.coords(self.id)

    def engine_down(self, evt):
        if self.down:
            self.down = False
            self.engine_y = 0
            self.canvas.itemconfig(self.engine_y_text, text='Main Engine: off')
        else:
            self.down = True
            self.canvas.itemconfig(self.engine_y_text, text='Main Engine: on')

    def engine_left(self, evt):
        if self.right or self.left:
            self.left = False
            self.right = False
            self.engine_x = 0
            self.canvas.itemconfig(self.engine_x_text, text='Thrusters: off')
        else:
            self.left = True
            self.canvas.itemconfig(self. engine_x_text, text='Thrusters: left')

    def engine_right(self, evt):
        if self.right or self.left:
            self.left = False
            self.right = False
            self.engine_x = 0
            self.canvas.itemconfig(self.engine_x_text, text='Thrusters: off')
        else:
            self.right = True
            self.canvas.itemconfig(self.engine_x_text, text='Thrusters: right')
