from Tkinter import *
import time
import random
import os
from Lander_1 import Lander
from Lander_2 import Lander_2


class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

GRAVITY = 0.0005 #setting up the variables for the lander
ENGINE_POWER = -0.00003
THRUSTER_POWER = 0.00001
MAXIMUM_ENGINE_POWER = -0.002
MAXIMUM_THRUSTER_POWER = 0.0001
game_started = False
paused = False
pause_tick = 0
continue_tick = 0
on_platform = False
start_tick = 0
SavedCoords = False
window_height = 500
window_width = 1000
fuel_Number = 2000
Lunar_2_running = False

class bcolors:  #setting a class for terminal coloring schemes
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'


class Game: # the class for the actual game screen
    def __init__(self):
        global window_width
        global window_height
        self.game = Tk() #game screen
        self.game.attributes('-alpha', .80)
        self.game.title("")
        self.canvas = Canvas(self.game, width=700, height=500, highlightthickness=0)
        self.canvas.pack()
        self.canvas.focus_set()
        self.game.update()
        self.canvas_height = window_width
        self.canvas_width = window_height
        self.sprites = []
        self.running = True

    def mainloop(self):
        if pause_tick == 2 and Lunar_2_running == True:
            Lunar_2.Pause(game)
            Lunar_2_running = False
        elif pause_tick != 2:
            Lunar_2_running = True
        while 1:#updating the postition of the Lunar Lander
            if paused == False:
                if self.running == True:
                    for sprite in self.sprites:
                        sprite.move(self.game, paused, SavedCoords, GRAVITY, ENGINE_POWER, THRUSTER_POWER, MAXIMUM_ENGINE_POWER, MAXIMUM_THRUSTER_POWER)
            self.game.update_idletasks()
            self.game.update()
            time.sleep(0.01)



class Start_Screen: # The "main menu" screen, which alows you to open options, start the game, and quit.
    def __init__(self, game):
        self.game = game #the game canvas
        self.canvas = game.canvas
        self.canvas.pack()
        self.canvas_height = window_width
        self.canvas_width = window_height
        self.title = self.canvas.create_text(350,150, font=('Courier New', 25), text ='Lunar Lander')
        self.title = self.canvas.create_text(350,170, font=('Courier New', 10), text ='By Livi Poon')
        self.start = self.canvas.create_text(350,300, font=('Courier New', 15), activefill="green",text ='Start',fill="black",tag='start')
        if continue_tick >= 1:
            self.start = self.canvas.create_text(350,275, font=('Courier New', 15),activefill="green", text ='Countinue?',fill="black",tag='countinue')
        self.start = self.canvas.create_text(350,325, font=('Courier New', 15),activefill="green", text ='Options',fill="black", tag='options')
        self.start = self.canvas.create_text(350,350, font=('Courier New', 15),activefill="green", text ='Quit',fill="black",tag='quit')
        self.canvas.tag_bind('start', '<ButtonPress-1>', self.callback)
        self.canvas.tag_bind('countinue', '<ButtonPress-1>', self.callback2)
        self.canvas.tag_bind('quit', '<ButtonPress-1>', self.quit_action)
        self.canvas.tag_bind('options', '<ButtonPress-1>', self.Options)

    def Options(self,evt):
        self.game.canvas.delete("all")
        Options(self)


    def callback(self, evt):
        global paused
        global SavedCoords
        global pause_tick
        SavedCoords = False
        self.game.canvas.delete("all")
        lander = Lander(self.game,SavedCoords, fuel_Number)
        lander_2 = Lander_2(self.game,SavedCoords, fuel_Number)
        platfrom = Platform(self.game)
        self.game.sprites = []
        self.game.sprites.append(lander_2)
        self.game.sprites.append(lander)
        Lunar_2_running = True
        pause_tick = 0
        paused = False

    def callback2(self, evt):
        global paused
        global pause_tick
        self.game.canvas.delete("all")
        lander = Lander(self.game,SavedCoords, GRAVITY, ENGINE_POWER, THRUSTER_POWER, MAXIMUM_ENGINE_POWER, MAXIMUM_THRUSTER_POWER, game_started, paused, pause_tick, continue_tick, fuel_Number)
        lander_2 = Lander_2(self.game,SavedCoords, GRAVITY, ENGINE_POWER, THRUSTER_POWER, MAXIMUM_ENGINE_POWER, MAXIMUM_THRUSTER_POWER, game_started, paused, pause_tick, continue_tick, fuel_Number)
        platfrom = Platform(self.game)
        self.game.sprites = []
        self.game.sprites.append(lander)
        self.game.sprites.append(lander_2)
        Lunar_2_running = True
        pause_tick = 0
        paused = False

    def quit_action(self, evt):
        self.game.game.destroy() #destroy the "top" screen


class Platform: #the tiny platform at which the lander can land on
    def __init__(self, game):
        on_platform = False
        self.canvas = game.canvas
        self.x1 = random.randrange(50,650)
        self.x2 = self.x1 + 50
        self.y1 = 480
        self.y2 = 490
        self.platform_id = self.canvas.create_rectangle(self.x1,self.y1,self.x2,self.y2)

class Options:
    def __init__(self, game):
        self.game = game#accesing all of the specs in the game class to be the specs in the Lander class.
        self.canvas = game.canvas
        self.title = self.canvas.create_text(350,150, font=("Courier New", 20),fill='blue', text='Options')
        self.mainmenu = self.canvas.create_text(35, 490, font=("Courier New", 12),activefill= 'green', text='Main Menu', tag='main')
        self.Video = self.canvas.create_text(350,200, font=("Courier New", 15), activefill = 'green', text='Video', tag='video')
        self.Gameplay = self.canvas.create_text(350,225, font=("Courier New", 15), activefill = 'green', text='Gameplay', tag='gameplay')
        self.Controls = self.canvas.create_text(350,250, font=("Courier New", 15), activefill = 'green', text='Controls/Keybindings', tag='controls')
        self.About = self.canvas.create_text(350,275, font=("Courier New", 15), activefill = 'green', text='About Developer',tag='about')
        self.canvas.tag_bind('main', '<ButtonPress-1>', self.Start_Screen_Boot)
        self.canvas.tag_bind('about', '<ButtonPress-1>', self.about)
        self.canvas.tag_bind('video', '<ButtonPress-1>', self.video)
        self.canvas.tag_bind('gameplay', '<ButtonPress-1>', self.gameplay)
        self.canvas.tag_bind('controls', '<ButtonPress-1>', self.controls)

    def Start_Screen_Boot(self, evt):
        self.game.canvas.delete("all")
        start_screen = Start_Screen(self.game)
        global paused
        global SavedCoords
        paused = True
        SavedCoords = True

    def about(self, evt):
        self.game.canvas.delete("all")
        self.about_text = self.canvas.create_text(350,40, font=("Courier New", 10), text='Livi Poon is a thriving student at the Nueva School, and is currently in 7th grade.\nHe enjoys Astronomy, Cosmology, Climate Science, Drawing, Swimming, Programming, Math, and Science.\nHe has participated in two hackathons (at Make Hacks) and was nominated\nthe youngest participant two times in a row; he was mentioned in pictures multiple times as well.\nHe also helped to influence the 4th grade curriculum (at The Nueva School) by integrating catapult simulations into\ntheir computer science curriculum and creating a whole entire new one week class about programming.')
        self.mainmenu = self.canvas.create_text(35, 490, font=("Courier New", 12),activefill= 'green', text='Main Menu', tag='main')

    def video(self, evt):
        global window_width
        global window_height
        self.game.canvas.delete("all")
        self.title = self.canvas.create_text(40,20, font=("Courier New", 20),fill='blue', text='VIDEO')
        self.mainmenu = self.canvas.create_text(35, 490, font=("Courier New", 12),activefill= 'green', text='Main Menu', tag='main')

        self.height = self.canvas.create_text(144,40, font=("Courier New", 12), text='Height: %s' % window_height)
        self.height_Plus = self.canvas.create_text(195,34, font=("Courier New",12), text='|+|', activefill='green', tag='add_window_height')
        self.height_Minus = self.canvas.create_text(195,44, font=("Courier New",12), text='|-|', activefill='green', tag='minus_window_height')
        self.canvas.tag_bind('add_window_height', '<ButtonPress-1>',self.add_window_height)
        self.canvas.tag_bind('minus_window_height', '<ButtonPress-1>',self.minus_window_height)

        self.width = self.canvas.create_text(144,70, font=("Courier New", 12), text='Width: %s' % window_width)
        self.width_Plus = self.canvas.create_text(195,64, font=("Courier New",12), text='|+|', activefill='green', tag='add_window_width')
        self.width_Minus = self.canvas.create_text(195,74, font=("Courier New",12), text='|-|', activefill='green', tag='minus_window_width')
        self.canvas.tag_bind('add_window_width', '<ButtonPress-1>',self.add_window_width)
        self.canvas.tag_bind('minus_window_width', '<ButtonPress-1>',self.minus_window_width)


    def gameplay(self,evt):
        self.game.canvas.delete("all")
        self.title = self.canvas.create_text(60,20, font=("Courier New", 20),fill='blue', text='GAMEPLAY')
        global fuel_Number
        global GRAVITY
        global ENGINE_POWER
        global THRUSTER_POWER
        global MAXIMUM_ENGINE_POWER
        global MAXIMUM_THRUSTER_POWER

        self.fuel = self.canvas.create_text(144,40, font=("Courier New", 12), text='Fuel: %s' % fuel_Number)
        self.fuel_Plus = self.canvas.create_text(195,34, font=("Courier New",12), text='|+|', activefill='green', tag='add_fuel')
        self.fuel_Minus = self.canvas.create_text(195,44, font=("Courier New",12), text='|-|', activefill='green', tag='minus_fuel')
        self.canvas.tag_bind('add_fuel', '<ButtonPress-1>',self.add_fuel)
        self.canvas.tag_bind('minus_fuel', '<ButtonPress-1>',self.minus_fuel)

        self.gravity = self.canvas.create_text(193,70, font=("Courier New", 12), text='Power of Gravity: %s' % GRAVITY)
        self.gravity_Plus = self.canvas.create_text(290,64, font=("Courier New",12), text='|+|', activefill='green', tag='add_gravity')
        self.gravity_Minus = self.canvas.create_text(290,74, font=("Courier New",12), text='|-|', activefill='green', tag='minus_gravity')
        self.canvas.tag_bind('add_gravity', '<ButtonPress-1>',self.add_gravity)
        self.canvas.tag_bind('minus_gravity', '<ButtonPress-1>',self.minus_gravity)

        self.thruster_p = self.canvas.create_text(183,100, font=("Courier New", 12), activefill='green', text='Thruster Power: %s' % THRUSTER_POWER)
        self.thruster_p_Plus = self.canvas.create_text(270,94, font=("Courier New",12), text='|+|', activefill='green', tag='add_thruster_p')
        self.thruster_p_Minus = self.canvas.create_text(270,104, font=("Courier New",12), text='|-|', activefill='green', tag='minus_thruster_p')
        self.canvas.tag_bind('add_thruster_p', '<ButtonPress-1>', self.add_thruster_p)
        self.canvas.tag_bind('minus_thruster_p', '<ButtonPress-1>', self.minus_thruster_p)

        self.mainmenu = self.canvas.create_text(35, 490, font=("Courier New", 12),activefill= 'green', text='Main Menu', tag='main')

    def add_fuel(self,evt):
        global fuel_Number
        fuel_Number += 100
        print("")
        print("Fuel.Capacity", fuel_Number)

    def minus_fuel(self,evt):
        global fuel_Number
        fuel_Number -= 100
        print("")
        print("Fuel.Capacity", fuel_Number)

    def add_gravity(self,evt):
        global GRAVITY
        GRAVITY += 0.0001
        print("")
        print("Gravity.Power", GRAVITY)

    def minus_gravity(self,evt):
        global GRAVITY
        GRAVITY -= 0.0001
        print("")
        print("Gravity.Power", GRAVITY)

    def add_thruster_p(self,evt):
        global THRUSTER_POWER
        THRUSTER_POWER += 0.00001
        print("")
        print("Thruster.Power", THRUSTER_POWER)

    def minus_thruster_p(self,evt):
        global THRUSTER_POWER
        THRUSTER_POWER -= 0.00001
        print("")
        print("Thruster.Power", THRUSTER_POWER)

    def add_MAX_thruster_p(self,evt):
        global MAXIMUM_THRUSTER_POWER
        MAXIMUM_THRUSTER_POWER += 0.0001
        print("")
        print("MAX.Thruster.Power", MAXIMUM_THRUSTER_POWER)

    def minus_MAX_thruster_p(self,evt):
        global MAXIMUM_THRUSTER_POWER
        MAXIMUM_THRUSTER_POWER -= 0.0001
        print("")
        print("MAX.Thruster.Power", MAXIMUM_THRUSTER_POWER)

    def controls(self,evt):
        self.game.canvas.delete("all")
        self.mainmenu = self.canvas.create_text(35, 490, font=("Courier New", 12),activefill= 'green', text='Main Menu', tag='main')

    def add_window_height(self, evt):
        global window_height
        window_height += 10
        print("")
        print("Window.Height" , window_height)

    def minus_window_height(self, evt):
        global window_height
        window_height -= 10
        print("")
        print("Window.Height_" , window_height)

    def add_window_width(self, evt):
        global window_width
        window_width += 10
        print("Window.Width_",window_width)

    def minus_window_width(self, evt):
        global window_width
        window_width -= 10
        print("Window.Width_",window_width)


g = Game()
start_screen = Start_Screen(g)
g.mainloop()
