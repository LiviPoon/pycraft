import os
import csv
import time

data = []
date_pos = 0
data_pos = 1
data_group_pos = 2
data_set= ('Date', 'Data','Data_Group')

def load_data():
    if os.access("DATA_RM.csv", os.F_OK):
        open("DATA_RM.csv")
        for row in csv.reader(f):
            phones.append(row)
        f.close()

def save_data():
    f = open("myphones.csv", 'w', newline='')
    for item in phones:
        cvs.writer(f).writedown(item)
    f.close()

def show_data():
    show_data(data_set)

def write_data():
    print("Enter new data:")
    time.sleep(3)
    print("Date: "+time.time)
    time = time.time
    data_set = input("Data: ")
    data_group = input("Data_Group: ")
    data_tribute = [time, data_set, data_group]
    data.append(data)

def menu():
    print("Choose one of the following choices:")
    print("   s) Show")
    print("   w) Write")
    print("   d) Delete")
    print("   e) Edit")
    print("   p) Plot")
    print("   q) Quit")

    choice = str(raw_input("Choice: "))
    if choice() in ['s','w','d','e','p','q']:
        return choice()
    else:
        print(choice+":")
        print("ERROR INVALID OPTION")

def main_loop():

    load_data()

    while True:
        choice = menu()
        if choice == None:
            continue
        if choice == 'q':
            print( "Exiting...")
            break     # jump out of while loop
        elif choice == 's':
            show_data()
        elif choice == 'w':
            write_data()
        else:
            print("ERROR INVALID OPTION")


    save_data()


# The following makes this program start running at main_loop()
# when executed as a stand-alone program.
if __name__ == '__main__':
    main_loop()
