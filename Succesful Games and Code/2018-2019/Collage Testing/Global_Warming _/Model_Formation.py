timeStep = 100           # years
waterDepth = 4000        # meters
L = 1350                 # Watts/m2
albedo = 0.3
epsilon = 1
sigma = 5.67E-8          # W/m2 K4

dHeatContent/dt = L*(1-alpha)/4 - epsilon * sigma * T^4
T[K] = HeatContent [J/m2] / HeatCapacity [J/m2K]
HeatContent(t+1) = HeatContent(t) + dHeatContent/dT * TimeStep
