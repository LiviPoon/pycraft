from statistics import mean
from statistics import stdev
import random
numList = []
random.seed(150)
for i in range(0,25):
    numList.append(round(100*random.random(),1))
#%%
def problem4_2(ran_list):
    """ Compute the mean and standard deviation of a list of floats """
    numList2 = []
    mean_list = mean(ran_list)
    print(mean_list)
    numList2.append(stdev(ran_list)) #stdev function for standard deviation
    print(str(numList2[0]))
