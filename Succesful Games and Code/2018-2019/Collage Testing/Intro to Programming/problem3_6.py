import csv
import sys

def problem3_6():
    infilename = sys.argv[1]
    outfilename = sys.argv[2]
    infile = open(infilename)
    outfile = open(outfilename, "w")
    for line in infile:
        outfile.write(str(len(line.strip("\n")))+"\n")
    infile.close()
    outfile.close()
problem3_6()
