import sys
import os
from random import randint
from time import sleep
from weatherClass import Weather

writingSpeed =0.060
# Things to implement:
# Class skills, more places, cave boss,  maybe some math, add interactive characters within the game, weather fatigue


def write(words):
    for char in words:
        sleep(writingSpeed)
        sys.stdout.write(char)
        sys.stdout.flush()
    sleep(1)
    print("")

class Character:
    def __init__(self):
        self.name = ""
        self.health = 100
        self.health_max = 100

    def doDamage(self, enemy):
        damage = min(max(randint(0, self.health) - randint(0, Enemy.health), 0), Enemy.health)
        Enemy.health = Enemy.health - damage
        if damage == 0:
            words = "%s evades %s's attack." % (Enemy.name, self.name)
            write(words)
        else:
            words = "%s hurts %s!" % (self.name, Enemy.name)
            write(words)
        return enemy.health <= 0

class Enemy(Character):
    def __init__(self, player):
        Character.__init__(self)
        self.name = "a goblin"
        self.health = randint(0, player.health)

class Player(Character):
    def __init__(self):
        Character.__init__(self)
        Weather.__init__(self)
        self.state = 'normal' #States: Fight, Sleep, Bargain, Normal, Fatigued, enemyBargain
        self.place = 'town' #Places: Town, Cave, Bar, mountainVillage, indianVillage
        self.type = ''
        self.loquaciousPerson = '' #People: Bard, Mage, Cyborg, Bartender, Indian, Looter, Demolition, Cow, Parrot, Grass
        self.energy = 50
        self.energy_max = 50
        self.health = 100
        self.health_max = 100
        self.coins = 0
        self.items = {}

    def changeWritingSpeed(self):
        speed = input("Input speed (slow, fast, normal) > ") #Function to tell what the player prefers, when it comes to how fast the typing is.

        if speed.upper() == "SLOW":
            writingSpeed = 0.1
        if speed.upper() == "FAST":
            writingSpeed = 0.0001
        if speed.upper() == "NORMAL":
            writingSpeed = 0.06

    def spinCoin(self, enemy):
        coinValue = randint(0,1)
        if self.state == 'bargain' or 'enemyBargain':
            if self.state == 'enemyBargain':
                if coinValue == 0:
                    words = "%s lose %sr bargain, and %s kills %s." % (Enemy.name)
                    write(words)
                    self.health = 0
                if coinValue == 1:
                    words = "%s win %sr bargain, the %s walks away, leaving %s with a bag of coins. %s gain 15 coins."
                    write(words)
                    self.coins += 15

            else:
                if coinValue == 0:
                    words = "%s lose %sr bet, and %s doesn't give %s anything" % (self.loquaciousPerson)
                    write(words)

                if coinValue == 0:
                    if self.loquaciousPerson == 'Bard': #The Bard you can meet at any time in the town, if you are in the right place at the right time.
                        self.items['Magical Book'] = randint(0,15)
                        self.loquaciousPerson = '' #Once done with bargaining you should reset the value of self.loquaciousPerson so to minimize errors

                    if self.loquaciousPerson == 'Mage': #The Mage should be the person you meet after the cave boss, and his staff should be rare
                        self.items['Magical Staff'] = randint(25,35)
                        self.loquaciousPerson = ''

                    if self.loquaciousPerson == 'Cyborg': #Cyborg should be only be acceseble at the end of a boss fight in the mountainVillage, and his pistol should be very rare
                        self.items['Eubiduem Pistol'] = randint(50,100)
                        self.loquaciousPerson = ''

                    if self.loquaciousPerson == 'Bartender': #The bartender is easy to find, because he is always in the bar. Therefore he either gives a weak weapon or some spare change.
                        bartenderValue = randint(0,1)
                        if bartenderValue == 0:
                            self.items['Beer Bottle'] = randint(0,5)
                            self.loquaciousPerson = ''
                        else:
                            self.coins += randint(0,5)
                            self.loquaciousPerson = ''

                    if self.loquaciousPerson == 'Indian': #The player must first find the villiage, kill the enemy that is taking them hostage. Then the player can bargain with the Indian.
                        self.items['Bow and Arrow'] = randint(10,20)
                        self.loquaciousPerson = ''

                    if self.loquaciousPerson == 'Looter': #The looter can be found pretty much anywhere, if you are in the right place in the right time.
                        self.coins += randint(10,30)
                        self.loquaciousPerson = ''

                    if self.loquaciousPerson == 'Demoltion': #Can be found in the caves, needs to be fought, and then the player can bargain
                        self.items['Bombs'] = randint(10,15)
                        self.loquaciousPerson = ''

                    if self.loquaciousPerson == 'Cow': #Can be found any where in the town, just be in the right place at the right time.
                        self.items['Shank'] = randint(0,5)
                        self.loquaciousPerson = ''

                    if self.loquaciousPerson == 'Parrot':#Parrot? What Parrot?
                        self.coins += 10
                        self.loquaciousPerson = ''

                    if self.loquaciousPerson == 'Grass':#OK, wtf? Are you ok?
                        self.coins += 1
                        self.loquaciousPerson = ''

                    words = "%s wins, and got a prize!" % (self.name)
                    write(words)

    def quit(self):
        words = "Why would %s quit? Is %s a weakling? Hmmm..." % (self.name, self.name)
        write(words)
        sleep(2)
        words = "FINE! %s dies a horrible death, one that %s won't remember even once %s is in the after life." % (self.name, self.name, self.name)
        write(words)
        self.health = 0


    def help(self):
        print(Commands.keys())

    def status(self):
        words = "You are in the %s state, and you health is %d/%d. Also you currently are in the %s, and you have %d energy left." %(self.state, self.health, self.health_max,self.place,self.energy)
        write(words)

    def bargain(self):
        if self.loquaciousPerson != '':
            self.state = 'bargain'
            words = "%s start to bargain with the %s" % (self.name,self.loquaciousPerson)

    def becomesTired(self):
        words = "%s feels tired." % self.name
        write(words)
        self.energy = max(1, self.energy - 1)

    def enemy_attacks(self):
      if self.enemy.do_damage(self):
          words = "%s was slaughtered by %s!!!\nR.I.P." % (self.name, self.enemy.name)
          write(words)

    def attack(self):
        if self.state != 'fight':
            words = "%s swats the air, without notable results." % (self.name, self.becomesTired)
            write(words)
        else:
          if self.do_damage(self.enemy):
            words = "%s executes %s!" % (self.name, self.enemy.name)
            write(words)
            self.enemy = None
            self.state = 'normal'
            if randint(0, self.health) < 10:
              self.health = self.health + 1
              self.health_max = self.health_max + 1
              words = "%s feels stronger!" % self.name
              write(words)
          else:
              self.enemy_attacks()

    def rest(self):
        self.restValue = randint(0,100)
        if self.place == 'town': #Depending on the place the senario changes.
            if self.state == 'sleep': #The sleepy state should be rare, and should only be given to the player if its night timeself.
                words = " Thanks! %s finally relizes that he is dumb, and %s actually needs sleep! Wow, what a relief. %s is really sleepy and %s falls into a deep trance right in the middle of the street. A cold wet and damp street, probably with all kinds of dung, piss, and beatles. Good job! %s restores all of his energy. " % (self.name, self.name, self.name, self.name, self.name)
                write(words)
                self.energy = self.energy_max

            elif self.state == 'bargain': #The player should only be in this state in the town, unless he encounters a enemy willing to bargain the playes life for something else. Which the chances will be slim.
                if self.restValue < 50:
                    words = "%s! Why are you not bargaining! Get your %s head in the game %s" % (self.name, self.type)
                    write(words)
                else:
                    words = "%s colapses and takes a snore right in front of the bargainer. %s loses the bargain." % (self.name, self.name )
                    write(words)
                    self.state = 'normal'

            elif self.state == 'fight': #The player should not encounter enemies that much in the town square.
                words = "First of all, why is %s fighting in the middle of a populated town? With CHILDREN! CHILDREN, PURE INNOCENT CHILDREN. " % (self.name)
                write(words)
                sleep(2)
                words = "%s falls and dies. Deal with it %s." % (self.name, self.type)
                write(words)

            elif self.state == 'normal':
                words = "%s fall on to the ground, the dirty ground. Ground that has seen the worst of the out houses, if those even still exist. While %s are resting a cow walks by, don't ask me why, but it takes the biggest dump right over %sr face. Wet, thick, moist dung right on %s's face. Oh and by the way it might have gone into %s's mouth, hard to tell. %s loses 10 energy" % (self.name, self.name, self.name, self.name, self.name)
                write(words)
                self.energy -= 10

            elif self.state == 'Fatigued':
                words = ""

        if self.place == 'cave':
            if self.state == 'normal':
                if self.restValue < 25:#Depending on the restValue the player might encounter an enemy, and not gain any energy. Or he could gain 5 health and 5 energy.
                    self.enemy = Enemy(self)
                    words = "%s encounters %s!" % (self.name, self.enemy.name)
                    write(words)
                    self.state = 'fight'
                else:
                    words = "%s tries to rest, and surprizingly %s is able to regain some health!" % (self.name, self.name)
                    write(words)
                    if self.energy < self.energy_max:
                        self.energy += 5
                    if self.health < self.health_max:
                        self.health += 5


            elif self.state == 'sleep': #The sleepy state should be rare, and should only be given to the player if its night timeself.
                words = "%s is really sleepy, and %s falls into a deep trance. %s restores all of %s's energy" % (self.name, self.name, self.name, self.name)
                write(words)
                self.energy = self.energy_ma

            elif self.state == 'fight':
                if self.restValue < 80: #Depending on what the restValue is you will either lose health or gain some energy
                    words = "%s sits on the ground, when %s does %s hears the whistle of a club coming right at %s's head. WHAM. %s gets hit in the head, with a wooden club. The club smashes into %s's head and splinters go everywhere. %s loses 10 health. While recovering %s collects a wooden shank, made from a broken piece of wood." % (self.name,self.name,self.name,self.name,self.name,self.name,self.name,self.name)
                    write(words)
                    self.health -= 10
                    self.items["Wooden Shank"] = 4

                else:
                    words =  "%s dies from external bleeding from a wooden shank %s stabbed into the back of %s's neck." % (self.name, self.enemy, self.name)
                    write(words)


            elif self.restValue == 0:
                words = "%s is woken up by %s's own dream, and could not sleep. %s did not get any sleep. Therefore %s's Energy has not changed." % (self.name,self.name, self.name,self.name)
                write(words)

            else:
                words = "%s tries to rest, and surprizingly %s was able to regain some health!" % (self.name, self.name)
                write(words)
                if self.energy < self.energy_max:
                    self.energy += 5
                if self.health < self.health_max:
                    self.health += 5

    def explore(self):
        self.exploreValue = randint(0,10)

        if self.place == 'town':
            if self.state == 'sleep':
                words = "%s is tired, and should sleep instead of exploring more and more. %s loses 20 energy." % (self.name, self.name)
                write(words)

            elif self.state == 'bargain':
                words = "%s is currently bargaining, how do you think he can explore? Are you dumb?" % (self.name)
                write(words)

            elif self.state == 'fight':
                words = "%s, seriously, ugh. %s tries to explore, while %s is attacking. %s loses 10 health." % (self.name, self.name, self.name, self.name)
                write(words)


            elif self.state == 'normal':
                if self.exploreValue > 7:
                    self.loquaciousPerson = 'Grass'
                    words = "%s runs into Grass. %d can bargain with it..." % (self.name,self.name)
                    write(words)

                if self.exploreValue == 5:
                    words = "%s finds a cave, a wet smelly dark and damp cave." % (self.name)
                    write(words)
                    self.place = 'cave'
                    self.becomesTired()

                if self.exploreValue == 2:
                    words = "%s encounters "
                else:
                    words = "%s finds a really borring cave, no smoke or anything. Just a barren cave." % (self.name)
                    write(words)
                    self.place = 'cave'
                    self.becomesTired()


            elif self.state == 'Fatigued':
                pass

            else:
                pass

        if self.place == 'cave':

            if self.state == 'sleep':
                pass

            elif self.state == 'bargain':
                pass

            elif self.state == 'fight':
                pass

            elif self.state == 'normal':
                pass

            elif self.state == 'Fatigued':
                pass

            else:
                pass



    def flee(self):
        self.fleeValue = randint(0,10)

        if self.place == 'town':
            if self.state == 'sleep':
                pass

            elif self.state == 'bargain':
                pass

            elif self.state == 'fight':
                pass

            elif self.state == 'normal':
                pass

            elif self.state == 'Fatigued':
                pass

            else:
                pass

        if self.place == 'cave':
            if self.state == 'normal':
                words = "%s is not in a battle, therefore %s can't flee from anything. %s loses 5 energy." % (self.name, self.name, self.name)
                write(words)
                self.energy -= 5

            if self.state == 'fight':
                if self.fleeValue >= 5:
                    words = "%s flees the battle. While fleeing %s loses 10 energy." % (self.name, self.name)
                    write(words)
                    self.state == 'normal'
                if self.fleeValue < 5:
                    words = "%s tries to flee, but end up tripping on a stray rock." % (self.name)
                    write(words)

Commands = {
  'quit': Player.quit,
  'help': Player.help,
  'status': Player.status,
  'rest': Player.rest,
  'explore': Player.explore,
  'flee': Player.flee,
  #'attack': Player.attack,
  }
