//
//  main.cpp
//  CtoFConverter
//
//  Created by Enerson Poon on 10/28/18.
//  Copyright © 2018 Enerson Poon. All rights reserved.
//

#include <iostream>
using namespace std;

int main() {
    // insert code here...
    int f;
    int c;
    cout << "Enter amount of Celsius: ";
    cin >> c;
    f = c/(9/5)*c+32;
    cout << c << " Celsius equals, " << f << " Fahrenheit \n";
    
    return 0;
}
