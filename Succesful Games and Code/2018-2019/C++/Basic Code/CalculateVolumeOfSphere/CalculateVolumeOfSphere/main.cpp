//
//  main.cpp
//  CalculateVolumeOfSphere
//
//  Created by Enerson Poon on 10/28/18.
//  Copyright © 2018 Enerson Poon. All rights reserved.
//

#include <iostream>
#include <cmath>

using namespace std;

int main()
{
    int rad1;
    float volsp;
    cout << "\n\n Calculate the volume of a sphere :\n";
    cout << "---------------------------------------\n";
    cout<<" Input the radius of a sphere : ";
    cin>>rad1;
    volsp=(4*3.14*rad1*rad1*rad1)/3;
    cout<<" The volume of a sphere is : "<< volsp << endl;
    cout << endl;
    return 0;
}
