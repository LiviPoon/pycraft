//
//  main.cpp
//  astroids
//
//  Created by Enerson Poon on 10/30/18.
//  Copyright © 2018 Enerson Poon. All rights reserved.
//

#include <iostream>
#include <string>
#include <algorithm>

using namespace std;
#include "olcConsoleGameEngine.h"
//Notes:
// Velocity = (P2 - P1)/t Therefore P2 = Pt + P1
// Acceleration = (V2 - V1)/t Therefore V2 = At + V

class Povellesto_Asteroids : public olcConsoleGameEngine
{
public:
    Povellesto_Asteroids()
    {
        m_sAppName= l"Astroid"
        
    }
    
private:
    struct sSpaceObject //AKA asteroids
    {
        float x; //x coordinates
        float y; //y coordinates
        float dx; //x velocity
        float dy; //y velocity
        int nSize; //Size of Asteroids
    }
    
    vector<sSpaceObject> vecAsteroids;
protected:
    //called by the game engine
    virtual bool OnUserCreate()
    {
        vecAsteroid.push_back({20.0f, 20.0f, 8.0f, -6.0f, (int)16})
        //Update and Draw the Asteroids
        for (auto &a: vecAsteroids)
        {
            a.x += a.dx * fElapsedTime
            a.y += a.dy * fElapsedTime
            for (int x = 0; x = < a.nSize; x++)
                for (int y = 0; y = < a.nSize; y++)
                    Draw(a.x+x, a.y+y, PIXEL_QUARTER, FG_BLUE
        }
        return True;
    }
    virtual bool OnUserCreate(float fElapsedTime)
    {
        //clear screen
        Fill(0,0, ScreenWidth(), ScreenHeight(), PIXEL_SOLID, 0);
        return True;
    }
};

int main(int argc, const char * argv[]) {
    //Make a olcConsoleGameEngine app
    Povellesto_Asteroids game; //name of app
    game.ConstructConsole(160,100,8,8); //assigns size for the app, each # is measured in characters (each 8x8 pixels)
    game.Start(); //app starts
    return 0;
    
    
}
