# Message Receiver
import os
from socket import *
host = "192.168.1.14" # set to IP address of target computer
port = 13000
buf = 1024
addr = (host, port)
UDPSock = socket(AF_INET, SOCK_DGRAM)
UDPSock.bind(addr)


print "Waiting to receive messages..."
while True:
    (data, addr) = UDPSock.recvfrom(buf)
    print "Received message: " + data
    if data == "break":
        break

    data_s = raw_input("Enter message to send or type 'break': ")
    UDPSock.sendto(data_s, addr)
    if data_s == "break":
        break

UDPSock.close()
os._exit(0)
